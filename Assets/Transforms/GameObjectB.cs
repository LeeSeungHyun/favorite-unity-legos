using UnityEngine;
using System.Collections;

public static class GameObjectB 
{
    /// <summary>
    /// Method to find the very top component of a gameObject of your desired type
    /// <typeparam name="T">Any Component</typeparam>
    /// <param name="go">Any GameObject instance</param>
    /// <returns></returns>
    public static T FindInParents<T>(GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null) return comp;

        var t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }
        return comp;
    }
}
