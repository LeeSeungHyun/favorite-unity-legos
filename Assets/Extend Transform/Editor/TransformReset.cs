using UnityEditor;
using UnityEngine;


[CustomEditor(typeof (Transform))]
public class TransformReset : Editor
{
    public static Vector3 resetPosition = Vector3.zero;
    public static Vector3 resetRotation = Vector3.zero;
    public static Vector3 resetScale = Vector3.one;

    public override void OnInspectorGUI()
    {
        /*get the transform of the target gameobject*/
        var data = target as Transform;

        /*make the transform inspector looks like the default inspector*/
        //EditorGUIUtility.LookLikeControls();//obsolete
        //DrawDefaultInspector();//Show even more like local coords
        EditorGUI.indentLevel = 0;

        var position = EditorGUILayout.Vector3Field("Position", data.localPosition);
        var eulerAngles = EditorGUILayout.Vector3Field("Rotation", data.localEulerAngles);
        var scale = EditorGUILayout.Vector3Field("Scale", data.localScale);


        #region Buttons


        /*add the reset position, rotation and scale buttons*/
        EditorGUILayout.BeginHorizontal();
        
        GUILayout.Label("Reset", EditorStyles.miniLabel);

        GUI.color = Color.cyan;

        if (GUILayout.Button("Position", EditorStyles.miniButtonLeft)) { position = resetPosition; }
        if (GUILayout.Button("Rotation", EditorStyles.miniButtonMid)) { eulerAngles = resetRotation; }
        if (GUILayout.Button("Scale", EditorStyles.miniButtonRight)) { scale = resetScale; }
        GUI.color = Color.white;

        EditorGUILayout.EndHorizontal();


        #endregion


        //apply the modifications to the targets transform
        if (GUI.changed)
        {
            Undo.RecordObject(data, "Transform Change");
            data.localPosition = FormatVector(position);
            data.localEulerAngles = FormatVector(eulerAngles);
            data.localScale = FormatVector(scale);
        }
    }

    /*prevent the input of non numbers values*/

    private Vector3 FormatVector(Vector3 vector)
    {
        if (float.IsNaN(vector.x)) { vector.x = 0; }
        if (float.IsNaN(vector.y)) { vector.y = 0; }
        if (float.IsNaN(vector.z)) { vector.z = 0; }

        return vector;
    }
}