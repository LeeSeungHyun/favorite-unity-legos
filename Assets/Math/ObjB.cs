using UnityEngine;


public static class ObjB
{
    #region Gameobjects


   

    //Double click lego

    //  if (Input.GetKeyDown(KeyCode.F5))
    //      {
    //      //Delay key presses - not a double click
    //      //limit Quick Save key presses to one per 2 seconds
    //      if (Time.time - lastClickTime<delay)
    //      {
    //         // Debug.Log("Too Soon to save again " + (Time.time - lastClickTime)); 
    //          return;
    //      }
    //}


    #endregion

    /// <summary>
    /// Returns point around a circle or arc by degrees.
    /// EX. List[Vector3] arcPointsList = ObjB.GetPointsOnArc(argDeg, arcRadius, arcPointsAmt).ToList();
    /// </summary>
    /// <param name="arcDeg">0-360f</param>
    /// <param name="radius"></param>
    /// <param name="nPoints"></param>
    /// <returns></returns>
    public static Vector3[] GetPointsOnArc(float arcDeg, float radius, int nPoints)
    {
        var points = new Vector3[nPoints];

        float n = MathB.RemapValues(arcDeg, 0, 360, 0, 2); //n=2 (full circle) n=1 (half-circle)
        
        for (int i = 0; i < nPoints; i++)
        {   

            float angle = i * Mathf.PI * n / nPoints;

            //Adds an poriton of the angle amount to make an end point when n is less than 360 deg
            if (n < 2) angle += i * Mathf.PI * n / nPoints / (nPoints - 1);//spent a whole night on this line

            var pos = new Vector3(Mathf.Cos(angle) * radius, 0, Mathf.Sin(angle) * radius);

            points[i] = pos;

        }
        
        return points;
    }


    /// <summary>
    /// Returns an array of n points around a unit sphere.
    /// </summary>
    /// <param name="nPoints"></param>
    /// <returns></returns>
    public static Vector3[] GetPointsOnSphere(int nPoints)
    {
        var points = new Vector3[nPoints];

        float inc = Mathf.PI*(3 - Mathf.Sqrt(5));
        var off = 2/nPoints;

        for (int k = 0; k < nPoints; k++)
        {
            float y = k*off - 1 + off/2;
            float r = Mathf.Sqrt(1 - y*y);
            float phi = k*inc;

            points[k] = new Vector3(Mathf.Cos(phi)*r, y, Mathf.Sin(phi)*r);
        }

        return points;
    }
}