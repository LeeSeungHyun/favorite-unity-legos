﻿using UnityEngine;
using System.Collections;

public class DotProductUtility : MonoBehaviour
{

    public Transform objA;
    public Transform objB;

    public float aDotB;
	
	
	// Update is called once per frame
	void Update ()
    {
	    Debug.DrawRay(objA.position,objA.forward*4,Color.red);
        Debug.DrawRay(objB.position, objB.forward*4, Color.blue);

	    aDotB = Vector3.Dot(objA.forward, objB.forward);
    }
}
