using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsOnAnArcExample : MonoBehaviour
{
    [Range(0, 360)] public int arcDegrees = 180;
    int currentDegrees;
    public float radius = 10;
    public int amountOfPoints = 32;
    int currentPoints;
    public List<Vector3> myPoints;

	void Rebuild()
	{
	    myPoints = MathB.GetPointsOnArc(arcDegrees, radius, amountOfPoints);
	    currentPoints = amountOfPoints;
	    currentDegrees = arcDegrees;
	}
	

    void OnDrawGizmos()
    {
        if (currentPoints != amountOfPoints || currentDegrees != arcDegrees)
        {
            Rebuild();
        }

        if (myPoints.Count <= 0) return;

        for (int i = 0; i < myPoints.Count; i++)
        {
            Gizmos.color  =Color.red;
            Gizmos.DrawSphere(myPoints[i],.1f);
        }

    }
}
