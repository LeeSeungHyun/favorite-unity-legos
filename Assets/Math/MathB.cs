using System.Collections.Generic;
using UnityEngine;



public static class MathB
{

    public enum Shapes
    {
        HalfCircle = 0,
        Circle = 1,

    }

    public static Vector3[] CircleOfPositions(float radius, int amount)
    {
        Vector3[] objectPositions = new Vector3[amount];
        for (int i = 0; i < amount; i++)
        {
            //Circle Shape
            float angle = i * Mathf.PI * 2f / amount;
            Vector3 position = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * radius;
            objectPositions[i] = position;
        }
        return objectPositions;
    }

    public static List<GameObject> CircleOfGameObjects(GameObject pf, float radius, int amount, bool fullCircle)
    {
        List<GameObject> objects = new List<GameObject>(amount);
        for (int i = 0; i < amount; i++)
        {
            //Circle Shape
            float n = fullCircle ? 2 : 1;//2=full circle 1=half circle
            float angle = i * Mathf.PI * n / amount;
            Vector3 position = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * radius;
            var obj = Object.Instantiate(pf, position, Quaternion.identity) as GameObject;
            objects.Add(obj);

        }
        return objects;
    }

    /// <summary>
    /// Returns points around a circle or arc by degrees.
    /// EX. List[Vector3] arcPointsList = ObjB.GetPointsOnArc(argDeg, arcRadius, arcPointsAmt).ToList();
    /// </summary>
    /// <param name="arcDeg">0-360f</param>
    /// <param name="radius"></param>
    /// <param name="nPoints"></param>
    /// <returns></returns>
    public static List<Vector3> GetPointsOnArc(float arcDeg, float radius, int nPoints)
    {
        List<Vector3> points = new List<Vector3>();

        float n = MathB.RemapValues(arcDeg, 0, 360, 0, 2); //n=2 (2pi=full circle) n=1 ( pi=half-circle)

        for (int i = 0; i < nPoints; i++)
        {

            float angle = i * Mathf.PI * n / nPoints;

            //Adds an poriton of the angle amount to make an end point when n is less than 360 deg
            if (n < 2) angle += i * Mathf.PI * n / nPoints / (nPoints - 1);//<<spent a whole night on this line

            var pos = new Vector3(Mathf.Cos(angle) * radius, 0, Mathf.Sin(angle) * radius);

            points.Add(pos);

        }

        return points;
    }


    /// <summary>
    /// Returns an array of n points arounf a unit sphere.
    /// </summary>
    /// <param name="nPoints"></param>
    /// <returns></returns>
    public static Vector3[] GetPointsOnSphere(int nPoints)
    {
        var points = new Vector3[nPoints];

        float inc = Mathf.PI * (3 - Mathf.Sqrt(5));
        var off = 2 / nPoints;

        for (int k = 0; k < nPoints; k++)
        {
            float y = k * off - 1 + off / 2;
            float r = Mathf.Sqrt(1 - y * y);
            float phi = k * inc;

            points[k] = new Vector3(Mathf.Cos(phi) * r, y, Mathf.Sin(phi) * r);
        }

        return points;
    }



    /// <summary>
    /// Returns a float value based on Time.time.
    /// EX. light.intensity.LerpPingPing(.1,2,1)
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <param name="flashingSpeed">flashes per second</param>
    /// <returns></returns>
    public static float LerpPingPong(float from, float to, float flashingSpeed)
    {
        return Mathf.Lerp(from, to, Mathf.PingPong(Time.time*flashingSpeed, 1));
        //ex. currentColor = Color.Lerp(onColor, offColor, Mathf.PingPong(Time.time * FastflashingSpeed, 1));
    }

    //Remapping or Normalize
    /// <summary>
    /// Returns an unclamped, normailzed float between MinOUT and MaxOUT between the MinIN to MaxIN. DEFAULT OUT is (0,1). 
    ///</summary>
    public static float RemapValues(float currentValue, float MinIN, float MaxIN, float MinOUT = 0f, float MaxOUT = 1f)
    {
        //is unclamped
        var normalizedFloat = MinOUT + (currentValue - MinIN)*(MaxOUT - MinOUT)/(MaxIN - MinIN);
        return normalizedFloat;
    }


    //Physics Explode Lego
    /// <summary>
    /// Apply a force at a position to all rigidbodies within the radius.
    /// </summary>
    public static void ExplosiveForce(float explosivePower, Vector3 explosionPos, float explosiveRadius, ForceMode forceMode)
    {
        //Any colliders near by?
        Collider[] colliders = Physics.OverlapSphere(explosionPos, explosiveRadius);

        for (int i = 0; i < colliders.Length; i++)
        {
            Collider hit = colliders[i];
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null) rb.AddExplosionForce(explosivePower, explosionPos, explosiveRadius, 0, forceMode);
        }
    }
}
