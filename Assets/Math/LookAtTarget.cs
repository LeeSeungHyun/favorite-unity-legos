using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LookAtTarget : MonoBehaviour
{

    public Transform target;

	
	void Update ()
    {
	    transform.LookAt(target.transform , target.rotation * Vector3.up);
	   
        Debug.DrawRay(transform.position, transform.forward * 10, Color.white);
      
    }
}
