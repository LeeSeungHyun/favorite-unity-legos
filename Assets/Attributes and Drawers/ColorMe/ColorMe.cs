using UnityEngine;

public class ColorMe : PropertyAttribute
{
    public Color color;
    public ColorMe(float r,float g, float b, float a)
    {
        color = new Color(r,g,b,a);
    }
    
    public ColorMe(string hex8){color = HexToColorRGBA(hex8);}
    
    public Color32 HexToColorRGBA(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        byte a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, a);
    }
}
