using System.Collections.Generic;
using UnityEngine;

public class TextColorAttribute : MonoBehaviour
{

    [ColorMe("FF99FFFF")]
    public string colorMeString;

    [ColorMe(0, 1, 0, 1)]
    public Vector3 coloMerVector3;

    [ColorMe(0, 1, 1, 1)]
    public float colorMeFloat;

    [ColorMe(0,0,1,1)]
    public List<int> myIntList;




}
