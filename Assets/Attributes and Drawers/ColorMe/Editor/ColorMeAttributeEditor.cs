using UnityEngine;
using UnityEditor;


[CustomPropertyDrawer(typeof (ColorMe))]
public class ColorAttributeEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // First get the attribute since it contains the color
        ColorMe colorMe = attribute as ColorMe;
        //Color if we have one
        if (colorMe != null)GUI.color = colorMe.color;
        //Draw the field
        EditorGUI.PropertyField(position, property, label);
        //Return to default color
        GUI.color = Color.white;

    }
}