using UnityEngine;
using UnityEngine.UI;

public class PlayMovie : MonoBehaviour {

    //Not Mobile Compatible see: https://docs.unity3d.com/Manual/class-MovieTexture.html

    #if !UNITY_ANDROID
    void Start()
    {
        ((MovieTexture)GetComponent<RawImage>().mainTexture).Play();//..Plays movie but no sound
        GetComponent<AudioSource>().Play();//adds the sound
    }
    #endif
}
