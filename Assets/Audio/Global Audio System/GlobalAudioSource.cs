using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GlobalAudioSource : MonoBehaviour
{
    //This should automatically load any child source.
    //Add 5 or more childed/positionable AudioSources
    public List<AudioSource> audioSources;
    //Inspector feedback
    public int LoadedSources;

    public int timesCalled;
    //first assgn on start
    public SimpleAudioEvent elephantSound; //elephant 
    //public LoopedAudioEvent testLoopSound;//elephant <<New Idea

    public static GlobalAudioSource Instance { get; private set; }

    public int currentSource;

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this) { Destroy(gameObject); }
    }

    void Start()
    {
        //PlayLoopHere(testLoopSound);
        //PlaySimpleHere(elephantSound);
    }

    void OnEnable()
    {
        if (audioSources == null) { audioSources = new List<AudioSource>(); }
        else audioSources.Clear();

        //assign all child AudioSources to a list
        var sources = GetComponentsInChildren<AudioSource>();

        foreach (var s in sources) { audioSources.Add(s); }

        LoadedSources = audioSources.Count;

        currentSource = 0;
    }

    //** Future update make a method for many world looping LONG sounds ie pooler with management(AdvancedAudioEvent.

    /// <summary>
    /// Plays a SimpleAudioEvent at position Vector3.Zero. Meant for 2D non-looping sounds.
    /// </summary>
    public void PlaySimpleHere(SimpleAudioEvent simpleAudio)
    {
        //simpleAudio.is_2dSound = true;--removed so we can be more agnostic use simpleAudio in world too-make sure to manully adjust
        PlaySimpleHere(simpleAudio, Vector3.zero);
    }

    /// <summary>
    /// Plays a short non-looping SimpleAudioEvent at this World Position
    /// </summary>
    public void PlaySimpleHere(SimpleAudioEvent simpleAudio, Vector3 worldPos)
    {
        if (IsNullorEmpty(simpleAudio)) return;
        
        //get/set/load it
        var avail = AvailableSource();
        //Adjust to 2D or 3D sound
        avail.spatialBlend = simpleAudio.is_2dSound ? 0 : 1;
        //move it
        avail.transform.position = worldPos;
        //play it
        simpleAudio.Play(avail);
    }

    private AudioSource AvailableSource()
    {
        if (audioSources == null || audioSources.Count == 0)
        {
            Debug.Log("No Audio sources!!!");
            return null;
        }

        //by avail - could be null
        for (int i = 0; i < audioSources.Count; i++)
        {
            if (!audioSources[i].isPlaying)
            {
                //Debug.Log("Choosing Source " + i);
                currentSource = i; //last audio source used
                return audioSources[i];
            }
        }
        // return null;

        //in case null-optional-Tested good if hit switches repeatedly
        //by not last-cant be null

        //return 1,2,or 3 and not last used if none avail;
        if (currentSource >= audioSources.Count) currentSource = 0; //loop or reset back

        currentSource++;
        var index = currentSource - 1;
        //Debug.Log("Was going to be null. Used- "+index);
        return audioSources[index];
    }

    private bool IsNullorEmpty(SimpleAudioEvent simpleAudio)
    {
        //Removed and placed here unchanged <<new method
        if (!simpleAudio)
        {
            if (elephantSound) PlaySimpleHere(elephantSound);
            Debug.Log("No Simple Audio Event Found! ");
            return true; //if no SimpleAudio return;
        }

        if (simpleAudio.clips.Count == 0)
        {
            if (elephantSound) PlaySimpleHere(elephantSound);
            Debug.Log("No Simple Audio Event CLIP Found! " + simpleAudio.name);
            return true;
        }

        if (simpleAudio.loop) //Check for audio looping we dont want looping SFX here.
        {
            //Resource log warning
            Debug.Log("Dont use loop here!");
        }

        return false;

    }

}