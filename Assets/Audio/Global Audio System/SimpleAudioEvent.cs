using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AudioEvents/Simple")]

public class SimpleAudioEvent : AudioEvent
{
    public List<AudioClip> clips;

    [MinMaxRange(0,1)]
    public RangedFloat volume;

    [MinMaxRange(.1f, 3f)]
    public RangedFloat pitch;

    public bool loop;
    public bool is_2dSound;
    void Reset()
    {
        volume.maxValue = 1;
        volume.minValue = 1;
        pitch.minValue = 1;
        pitch.maxValue = 1;
        loop = false;
    }

    public override void Play(AudioSource source)
    {
        if (clips.Count == 0) return;

        source.clip = clips[Random.Range(0, clips.Count)];
        source.volume = Random.Range(volume.minValue, volume.maxValue);
        source.pitch = Random.Range(pitch.minValue, pitch.maxValue);
        source.loop = loop;
        source.Play();
        
    }

    public override void Stop(AudioSource source)
    {
        source.Stop();
    }

    /// <summary>
    /// Plays on the Global Audio Source.
    /// </summary>
    public override void Play2DSound()
    {
        GlobalAudioSource.Instance.PlaySimpleHere(this);
    }
    /// <summary>
    /// Plays on the Global Audio Source.
    /// </summary>
    public override void Play3DSound(Vector3 worlPos)
    {
        GlobalAudioSource.Instance.PlaySimpleHere(this, worlPos);
    }
}