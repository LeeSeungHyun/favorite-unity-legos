using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;


/// <summary>
/// Bryan's favorite web helper and scaping methods. Updated 21/11/2016
/// </summary>
public static class WebB
{
   
    /// <summary>
    /// Returns any links in the supplied URL. http://www.java2s.com/Code/CSharp/Network/GetLinksFromHTML.html
    /// </summary>
    /// <param name="url">http, https, ftp, file,</param>
    /// <returns></returns>
    public static StringCollection ScrapLinks(string url)
    {
        // Scrape links from url
        var links = new StringCollection();
        // 1.

        var w = new WebClient();
        string s = w.DownloadString(url);

        // 2.

        foreach (var i in Find(s)) { links.Add(i.Href); }
        return links;
    }

    public static List<LinkItem> Find(string file)
    {
        var list = new List<LinkItem>();

        // 1.
        // Find all matches in file.
        var m1 = Regex.Matches(file, @"(<a.*?>.*?</a>)", RegexOptions.Singleline);

        // 2.
        // Loop over each match.
        foreach (Match m in m1)
        {
            string value = m.Groups[1].Value;
            var i = new LinkItem();

            // 3.
            // Get href attribute.
            var m2 = Regex.Match(value, @"href=\""(.*?)\""", RegexOptions.Singleline);
            if (m2.Success) { i.Href = m2.Groups[1].Value; }

            // 4.
            // Remove inner tags from text.
            string t = Regex.Replace(value, @"\s*<.*?>\s*", "", RegexOptions.Singleline);
            i.Text = t;

            list.Add(i);
        }
        return list;
    }

    /// <summary>
    /// Returns a string of the RAW text from the HTML URL supplied. Useful for search scraping for links and data.
    /// source: http://stackoverflow.com/questions/16642196/get-html-code-from-website-in-c-sharp
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    public static string GetHTML(string url)
    {
        var request = (HttpWebRequest) WebRequest.Create(url);
        var response = (HttpWebResponse) request.GetResponse();

        if (response.StatusCode == HttpStatusCode.OK)
        {
            var receiveStream = response.GetResponseStream();
            StreamReader readStream = null;

            if (response.CharacterSet == null) { readStream = new StreamReader(receiveStream); }
            else
            { readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet)); }

            string data = readStream.ReadToEnd();

            response.Close();
            readStream.Close();
            return "<color=blue>RAW HTML found</color>\n" + data;
        }
        return "Was enpty or Error";
    }

    /// <summary>
    /// Gets links from HTML content . Used in conjuntion with GetHTML
    /// </summary>
    /// <param name="HtmlContent"></param>
    /// <returns></returns>
    public static StringCollection GetLinksFromHTML(string HtmlContent)
    {
        var links = new StringCollection();

        var AnchorTags = Regex.Matches(HtmlContent.ToLower(), @"(<a.*?>.*?</a>)", RegexOptions.Singleline);

        foreach (Match AnchorTag in AnchorTags)
        {
            string value = AnchorTag.Groups[1].Value;

            var HrefAttribute = Regex.Match(value, @"href=\""(.*?)\""", RegexOptions.Singleline);
            if (HrefAttribute.Success)
            {
                string HrefValue = HrefAttribute.Groups[1].Value;
                if (!links.Contains(HrefValue)) { links.Add(HrefValue); }
            }
        }

        return links;
    }


    public struct LinkItem
    {
        public string Href;
        public string Text;

        public override string ToString()
        {
            return "Href: " + Href + "\tText: " + Text;
        }
    }
}