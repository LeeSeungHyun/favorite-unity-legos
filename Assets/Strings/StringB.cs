using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

/// <summary>
/// A collection of Bryan's favorite string helpers and misc funtions. also see MathB , WebB
/// </summary>
public static class StringB
{
    public static class EnumUtil
{
    //found at:
    //  http://stackoverflow.com/questions/972307/can-you-loop-through-all-enum-values
    public static IEnumerable<T> GetValues<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }
}
    //Dictionary Lego 1a
    //Add a line of text to test
    public static string ToPrettyString<TKey, TValue>(this IDictionary<TKey, TValue> dict)
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("{");
        foreach (var pair in dict)
        {
            stringBuilder.Append(string.Format(" {0}={1} ", pair.Key, pair.Value));
        }
        stringBuilder.Append("}");
        return stringBuilder.ToString();
    }


    //Dictionary Lego 1b
    public static string ToDebugString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
    {
        return "{" + string.Join(",", dictionary.Select(kv => kv.Key + "=" + kv.Value).ToArray()) + "}";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"> folder path</param>
    /// <param name="ext">.png, .jpg </param>
    /// <returns></returns>
    public static Dictionary<int,Texture2D> GetAllEmotesFilesByExt(string path, string ext)
    {
        var dict = new Dictionary<int, Texture2D>();

        string[] files = System.IO.Directory.GetFiles(path, "*" + ext);

        foreach (var file in files)
        {
            //Debug.Log(file);
            string n = file.Replace(".png", "");
            n = n.Replace(path, "");
            int i;
            //Debug.Log(n);
            if (int.TryParse(n, out i))
            {
                //Debug.Log(i);

                dict[i]=GetImageFromFile(file);
            }
        }
        Debug.Log(dict.ToDebugString());
        return dict;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"> folder path</param>
    /// <param name="ext">.png, .jpg </param>
    /// <returns></returns>
    public static List<Texture2D> GetAllFilesByExt(string path,string ext)
    {
        var list = new List<Texture2D>();

        string[] files = System.IO.Directory.GetFiles(path, "*" + ext);

        foreach (var file in files)
        {
            list.Add(GetImageFromFile(file));
        }

        return list;
    }


    /// <summary>
    ///  Load the preview image at this path. Returns null.
    /// </summary>
    /// <param name="fullPath">Use full path with extention .png </param>
    /// <returns></returns>
    public static Texture2D GetImageFromFile(string fullPath)
    {
        if (!File.Exists(fullPath)) { return null; }

        Texture2D previewCopy = new Texture2D(2, 2, TextureFormat.RGBA32, false); 
        
        byte[] previewTex = File.ReadAllBytes(fullPath);

        previewCopy.LoadImage(previewTex, false);

        return previewCopy;
    }



    //void CreatePreviewImage(string saveName)
    //{
    //    //Maybe use Application.Takescreenshot to grab UI <but no size control
    //    if (createPreviewOnEndOfFrame) StartCoroutine(CreatePreviewImageOnEndOfFrame(saveName));
    //    else CreatePreviewNow(saveName);

    //}
    //IEnumerator CreatePreviewImageOnEndOfFrame(string saveName)
    //{
    //    //we take the render shot as late as possible to ensure everything rendered

    //    yield return new WaitForEndOfFrame();
    //    CreatePreviewNow(saveName);
    //}

    //void CreatePreviewNow(string saveName)
    //{
    //    RenderTexture rt = new RenderTexture(512, 265, 24);
    //    Camera.main.targetTexture = rt;
    //    Camera.main.Render();

    //    Texture2D texture2D = new Texture2D(512, 256, TextureFormat.RGB24, false);
    //    RenderTexture.active = rt;
    //    texture2D.ReadPixels(new Rect(0, 0, 512, 265), 0, 0);

    //    //release
    //    Camera.main.targetTexture = null;
    //    RenderTexture.active = null; //added to avoid errors
    //    DestroyImmediate(rt);

    //    byte[] bytes = texture2D.EncodeToPNG();

    //    string path = Application.persistentDataPath + "/" + currentProfileName + "/" + saveName;
    //    File.WriteAllBytes(path + ".png", bytes);
    //}
}
