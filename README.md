*** 
### What is this repository for? ###

* # A unique collection of methods, code, and custom systems to make life easier. #
* All code was tested/written in Unity 5.5.1 with C#
* Feel free to use at your own risk.
* Collection 1.0 -  Started creating this repo on February 18th, 2017


### How do I get set up? ###

* Browse the linked images or browse folders

![GetPointsOnArc.gif](https://bitbucket.org/repo/yxXXyX/images/4139826181-GetPointsOnArc.gif)

* [Points on an Arc](https://bitbucket.org/skyway_interactive/favorite-unity-legos/src/b90a59b836eefae96b41eba3aa92a0f261783f67/Assets/Math/MathB.cs?at=master&fileviewer=file-view-default)

![progressBarsloop.gif](https://bitbucket.org/repo/yxXXyX/images/3183330668-progressBarsloop.gif) ![progressBarAttributeSimple.png](https://bitbucket.org/repo/yxXXyX/images/2770982024-progressBarAttributeSimple.png)

* [Progress Bar Attribute](https://bitbucket.org/skyway_interactive/favorite-unity-legos/src/47013a997e219fd46725c21b74dd3a24bb3773e0/Assets/Attributes%20and%20Drawers/Progress%20Bar/?at=master)

![customRangedFloats.gif](https://bitbucket.org/repo/yxXXyX/images/3690810108-customRangedFloats.gif)

* Custom RangedFloats

### Who do I talk to? ###

* Repo owner or admin
* https://twitter.com/SkywayInteract